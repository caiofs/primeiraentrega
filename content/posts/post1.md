+++
title = 'Fantasma da série B'
date = 2024-10-10T14:02:07-03:00
draft = false
+++
### Fluminense e Corinthians são os gigantes desse ano que estão sofrendo com os resultados esse ano.
![Rebaixados](/images/rebaixados.png)

O tricolor carioca (atual campeão da libertadores) e o endividado alvinegro paulista (Semi-finalista da copa do Brasil e vivo na Sulamericana) são os dois gigantes do futebol brasileiro que estão quase caindo para a segunda divisão do campeonato brasileiro. Desde o começo do campeonato nenhum deles conseguiu uma boa sequência de jogos e já possuem grandes chances de serem rebaixados. 

A situação do Corinthians se agrava ainda por conta de suas dívidas com contratos anteriores e a construção de seu estádio, a Neo Química Arena. Apostando alto em Memphis Depay, astro holandês que veio sob grandes expectativas para o clube nesta temporada ainda não desmonstrou todo seu potencial e já recebe críticas e cobranças por parte da torcida mais apaixonada do país.
#### Corinthians precisa vencer 80% das próximas rodadas para se manter na série A!

