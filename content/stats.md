+++
title = 'Confira as estatísticas dos últimos 10 anos'
date = 2024-10-12T19:16:30-03:00
draft = false
+++
 2023: Campeão: Palmeiras (12); Artilheiro: Paulinho (Atlético Mineiro); Rebaixados: Santos Fc* (17º),  Goias (18º), Coritiba (19º) e América-MG (20º).  

 2022: Campeão: Palmeiras (11); Artilheiro: Cano (Fluminense); Rebaixados: Ceará (17º), Atlético-GO (18º), Avaí (19º) e Juventude (20º). 

 2021: Campeão: Atlético Mineiro (2); Artilheiro: Hulk (Atlético Mineiro); Rebaixados: Grêmio (17º), Bahia (18º), Sport (19º) e Chapecoense (20º). 

 2020: Campeão: Flamengo (7); Artilheiro(s): Luciano(SPFC) e Claudinho (RB Bragantino); Rebaixados: Vasco (17º), Goiás (18º), Coritiba (19º) e Botafogo (20º). 

 2019: Campeão: Flamengo (6); Artilheiro: Gabriel Barbosa (Flamengo); Rebaixados: Cruzeiro (17º), CSA (18º), Chapecoense (19º) e Avaí (20º). 

 2018: Campeão: Palmeiras (10); Artilheiro:Gabriel Barbosa (Santos FC); Rebaixados: América-MG (17º), Sport (18º), Vitória (19º) e Paraná (20º). 

 2017: Campeão: Corinthians (7); Artilheiro(s): Jô (Corinthians) e Dourado (Fluminense); Rebaixados: Coritiba (17º), Avaí (18º), Ponte Preta (19º) e Atlético-GO (20º). 

 2016: Campeão: Palmeiras (9); Artilheiro:Diego Souza (Sport), Pottker (Ponte Preta) e Fred (Atlético Mineiro); Rebaixados: Internacional (17º), Figueirense (18º), Santa Cruz (19º) e América-MG (20º). 

 2015: Campeão: Corinthians (6); Artilheiro: Ricardo de Oliveira (Santos FC); Rebaixados: Avaí (17º), Vasco (18º), Goiás (19º) e Joinville (20º). 

 2014: Campeão: Cruzeiro (4); Artilheiro: Fred (Fluminense); Rebaixados: Vitória (17º), Bahia (18º), Botafogo (19º) e Criciúma (20º). 